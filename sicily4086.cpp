//题目分析:NULL

//题目网址:http://soj.me/4086

#include <iostream>

using namespace std;

int main()
{
    int T;
    int a, b, c, k;
    bool flag;
    cin >> T;
    while (T--) {
        flag = false;
        cin >> a >> b >> c;
        for (int i = 1; i <= 15; i++) {
            k = i * 7 + c;
            if (k < 10) {
                continue;
            } else if (k > 100) {
                break;
            }
            if (k % 3 == a && k % 5 == b) {
                cout << k << endl;
                flag = true;
                break;
            }
        }
        if (!flag) 
            cout << "No answer" << endl;
    }
    
    return 0;
}                                 